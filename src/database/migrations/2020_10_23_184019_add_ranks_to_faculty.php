<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRanksToFaculty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uni_mbr')->table('faculty', function (Blueprint $table) {
            $table->unsignedBigInteger('rank_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uni_mbr')->table('faculty', function (Blueprint $table) {
            $table->dropColumn('rank_id');
        });
    }
}
