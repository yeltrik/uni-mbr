<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uni_mbr')->create('members', function (Blueprint $table) {
            $table->id();
            $table->string('email', 255)->nullable()->unique();
            $table->string('login', 255)->nullable()->unique();
            $table->string('name', 255)->unique();
            $table->string('number', 255)->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uni_mbr')->dropIfExists('members');
    }
}
