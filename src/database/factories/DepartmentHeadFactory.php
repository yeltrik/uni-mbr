<?php

namespace Yeltrik\UniMbr\database\factories;

use Yeltrik\UniMbr\app\models\DepartmentHead;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentHeadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DepartmentHead::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
