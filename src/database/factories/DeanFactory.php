<?php

namespace Yeltrik\UniMbr\database\factories;

use Yeltrik\UniMbr\app\models\Dean;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dean::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
