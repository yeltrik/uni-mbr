<?php

namespace Yeltrik\UniMbr\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yeltrik\UniMbr\app\models\Member;

class MemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->email,
            'login' => $this->faker->userName,
            'name' => $this->faker->name(),
            'number' => $this->faker->numberBetween(1000,1000000)
        ];
    }
}
