<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniOrg\app\models\Department;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = Staff::factory()
        ->count(2)
        ->for(Member::factory())
        ->make();

        foreach( $staff as $staff) {
            $staff->department()->associate(Department::query()->inRandomOrder()->firstOrFail());
            $staff->save();
        }
    }
}
