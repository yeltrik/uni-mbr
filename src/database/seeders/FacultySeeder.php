<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\Department;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faculty = Faculty::factory()
            ->count(2)
            ->for(Member::factory())
            ->make();

        foreach( $faculty as $faculty) {
            $faculty->department()->associate(Department::query()->inRandomOrder()->firstOrFail());
            $faculty->save();
        }
    }
}
