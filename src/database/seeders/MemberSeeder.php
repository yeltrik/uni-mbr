<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\Member;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = Member::factory()
            ->count(1000)
            ->create();
    }
}
