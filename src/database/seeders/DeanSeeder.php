<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\Dean;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\College;

class DeanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deans = Dean::factory()
        ->count(10)
        ->for(Member::factory())
        ->make();

        foreach( $deans as $dean) {
            $dean->college()->associate(College::query()->inRandomOrder()->firstOrFail());
            $dean->save();
        }
    }
}
