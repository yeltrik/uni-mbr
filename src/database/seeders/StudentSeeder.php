<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Student;
use Yeltrik\UniOrg\app\models\University;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $student = Student::factory()
            ->count(2)
            ->for(Member::factory())
            ->make();

        foreach( $student as $student) {
            $student->university()->associate(University::query()->inRandomOrder()->firstOrFail());
            $student->save();
        }
    }
}
