<?php

namespace Yeltrik\UniMbr\database\seeders;

use Illuminate\Database\Seeder;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniOrg\app\models\Department;

class DepartmentHeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departmentHeads = DepartmentHead::factory()
            ->count(5)
            ->for(Member::factory())
            ->make();

        foreach( $departmentHeads as $departmentHead) {
            $departmentHead->department()->associate(Department::query()->inRandomOrder()->firstOrFail());
            $departmentHead->save();
        }
    }
}
