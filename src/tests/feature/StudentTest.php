<?php

namespace Yeltrik\UniMbr\tests\feature;

use Yeltrik\UniMbr\app\models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentTest extends TestCase
{

    public function testRouteIndex()
    {
        $user = $this->getUser();
        $response = $this->actingAs($user, 'web')
            ->get(route('students.index', []));
        $response->assertStatus(200);
    }

    public function testRouteShow()
    {
        $user = $this->getUser();
        $student = Student::query()->inRandomOrder()->firstOrFail();
        $response = $this->actingAs($user, 'web')
            ->get(route('students.show', [$student]));
        $response->assertStatus(200);
    }

    public function getUser()
    {
        return User::query()->inRandomOrder()->firstOrFail();
    }

}
