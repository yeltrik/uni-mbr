@if($staff->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Staff
        </a>
        @foreach($staff as $staff)
            <a href="#"
               class="list-group-item list-group-item-action">{{$staff->member->name}}</a>
        @endforeach
    </div>
@endif
