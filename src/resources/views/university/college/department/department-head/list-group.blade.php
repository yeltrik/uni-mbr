@if($departmentHeads->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Department Heads
        </a>
        @foreach($departmentHeads as $departmentHead)
            <a href="#"
               class="list-group-item list-group-item-action">{{$departmentHead->member->name}}</a>
        @endforeach
    </div>
@endif
