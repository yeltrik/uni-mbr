@if($faculty->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Faculty
        </a>
        @foreach($faculty as $faculty)
            <a href="#"
               class="list-group-item list-group-item-action">{{$faculty->member->name}}</a>
        @endforeach
    </div>
@endif
