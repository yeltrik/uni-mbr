@if($deans->count() > 0)
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action active">
            Deans
        </a>
        @foreach($deans as $dean)
            <a href="#"
               class="list-group-item list-group-item-action">{{$dean->member->name}}</a>
        @endforeach
    </div>
@endif
