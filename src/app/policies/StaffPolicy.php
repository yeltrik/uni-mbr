<?php

namespace Yeltrik\UniMbr\app\policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Yeltrik\UniMbr\app\models\Staff;

class StaffPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        return TRUE;
    }

    public function view(User $user, Staff $staff)
    {
        return TRUE;
    }

}
