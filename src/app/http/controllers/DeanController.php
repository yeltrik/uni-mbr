<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\UniMbr\app\models\Dean;
use Illuminate\Http\Request;

class DeanController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $deans = Dean::all();
        $this->authorize('viewAny', Dean::class);
    }

    /**
     * @param Dean $dean
     * @throws AuthorizationException
     */
    public function show(Dean $dean)
    {
        $college = $dean->college;
        $member = $dean->member;
        $this->authorize('view', $dean);
    }

}
