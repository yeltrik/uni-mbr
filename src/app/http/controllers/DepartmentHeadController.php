<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\UniMbr\app\models\DepartmentHead;
use Illuminate\Http\Request;

class DepartmentHeadController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $departmentHeads = DepartmentHead::all();
        $this->authorize('viewAny', DepartmentHead::class);
    }

    /**
     * @param DepartmentHead $departmentHead
     * @throws AuthorizationException
     */
    public function show(DepartmentHead $departmentHead)
    {
        $this->authorize('view', $departmentHead);
    }

}
