<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\UniMbr\app\models\Staff;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $staff = Staff::all();
        $this->authorize('viewAny', Staff::class);
    }

    /**
     * @param Staff $staff
     * @throws AuthorizationException
     */
    public function show(Staff $staff)
    {
        $this->authorize('view', $staff);
    }

}
