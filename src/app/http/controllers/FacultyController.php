<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\UniMbr\app\models\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Faculty::class);
    }

    /**
     * @param Faculty $faculty
     * @throws AuthorizationException
     */
    public function show(Faculty $faculty)
    {
        $department = $faculty->department;
        $this->authorize('view', $faculty);
    }

}
