<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Yeltrik\UniMbr\app\models\Faculty;
use Yeltrik\UniMbr\app\models\Member;
use Yeltrik\UniMbr\app\models\Staff;
use Yeltrik\UniMbr\app\models\Student;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;
use Yeltrik\UniOrg\app\models\University;

class MemberController extends Controller
{

    /**
     * MemberController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'web']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Member::class);
    }

    /**
     * @param Member $member
     * @throws AuthorizationException
     */
    public function show(Member $member)
    {
        $this->authorize('view', $member);
    }

    /**
     * @param User $user
     * @param array $accType
     */
    public function register(User $user, array $accType)
    {
        $email = $user->email;
        $member = Member::query()
            ->where('email', '=', $email)
            ->first();

        if ( $member instanceof Member === FALSE ) {
            $member = new Member();
            $member->email = $email;
            $member->name  = $user->name;
            // TODO: Populate login, and number from Soap Auth
            $member->save();
        }

        // TODO: Determine College?
        $university = University::query()
            ->where('name', '=', 'Unknown')
            ->first();
        if ( $university instanceof University === FALSE ) {
            $university = new University();
            $university->name = 'Unknown';
            $university->save();
        }
        // TODO: Determine College?
        $college = College::query()
            ->where('name', '=', 'Unknown')
            ->first();
        if ( $college instanceof College === FALSE ) {
            $college = new College();
            $college->university()->associate($university);
            $college->name = 'Unknown';
            $college->save();
        }
        // TODO: Determine Department?
        $department = Department::query()
            ->where('name', '=', 'Unknown')
            ->first();
        if ( $department instanceof Department === FALSE ) {
            $department = new Department();
            $department->university()->associate($university);
            $department->college()->associate($college);
            $department->name = 'Unknown';
            $department->save();
        }

        if (array_key_exists('Faculty', $accType) && $accType['Faculty'] === 'TRUE') {
            $faculty = Faculty::query()
                ->where('member_id', '=', $member->id)
                ->first();
            if ($faculty instanceof Faculty === FALSE) {
                $faculty = new Faculty();
                $faculty->member()->associate($member);
                $faculty->department()->associate($department);
                $faculty->save();
            }
        }
        if (array_key_exists('Staff', $accType) && $accType['Staff'] === 'TRUE') {
            $staff = Staff::query()
                ->where('member_id', '=', $member->id)
                ->first();
            if ($staff instanceof Staff === FALSE) {
                $staff = new Staff();
                $staff->member()->associate($member);
                $staff->department()->associate($department);
                $staff->save();
            }
        }
        if (array_key_exists('Student', $accType) && $accType['Student'] === 'TRUE') {
            $student = Student::query()
                ->where('member_id', '=', $member->id)
                ->first();
            if ($student instanceof Student === FALSE) {
                $student = new Student();
                $student->university()->associate($university);
                $student->member()->associate($member);
                $student->save();
            }
        }
    }

}
