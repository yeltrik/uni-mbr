<?php

namespace Yeltrik\UniMbr\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Yeltrik\UniMbr\app\models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $students = Student::all();
        $this->authorize('viewAny', Student::class);
    }

    /**
     * @param Student $student
     * @throws AuthorizationException
     */
    public function show(Student $student)
    {
        $this->authorize('view', $student);
    }

}
