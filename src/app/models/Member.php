<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Yeltrik\UniMbr\database\factories\MemberFactory;

/**
 * Class Member
 *
 * @property int id
 * @property string email
 * @property string login
 * @property string name
 * @property int number
 *
 * @property Dean dean
 * @property DepartmentHead departmentHead
 * @property Faculty faculty
 * @property Staff staff
 * @property Student student
 *
 * @package Yeltrik\UniMbr\app\models
 */
class Member extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'members';

    public static function newFactory()
    {
        return new MemberFactory();
    }

    /**
     * @return HasOne
     */
    public function dean()
    {
        return $this->hasOne(Dean::class);
    }

    /**
     * @return HasOne
     */
    public function departmentHead()
    {
        return $this->hasOne(DepartmentHead::class);
    }

    /**
     * @return HasOne
     */
    public function faculty()
    {
        return $this->hasOne(Faculty::class);
    }

    /**
     * @return HasOne
     */
    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    /**
     * @return HasOne
     */
    public function student()
    {
        return $this->hasOne(Student::class);
    }

}
