<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniMbr\database\factories\StudentFactory;
use Yeltrik\UniOrg\app\models\University;

class Student extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'students';

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public static function newFactory()
    {
        return new StudentFactory();
    }

    public function university()
    {
        return $this->belongsTo(University::class);
    }

}
