<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\UniMbr\database\factories\FacultyFactory;
use Yeltrik\UniOrg\app\models\Department;

/**
 * Class Faculty
 *
 * @property int id
 * @property int member_id
 * @property int college_id
 * @property int rank_id
 *
 * @property Member member
 * @property Department department
 * @property Rank rank
 *
 * @package Yeltrik\UniMbr\app\models
 */
class Faculty extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'faculty';

    /**
     * @return BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * @return BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    /**
     * @return FacultyFactory
     */
    public static function newFactory()
    {
        return new FacultyFactory();
    }

    /**
     * @return BelongsTo
     */
    public function rank()
    {
        return $this->belongsTo(Rank::class);
    }

}
