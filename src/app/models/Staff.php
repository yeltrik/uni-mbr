<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniMbr\database\factories\StaffFactory;
use Yeltrik\UniOrg\app\models\Department;

class Staff extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'staff';

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public static function newFactory()
    {
        return new StaffFactory();
    }

}
