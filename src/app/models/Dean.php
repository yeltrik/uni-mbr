<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniMbr\database\factories\DeanFactory;
use Yeltrik\UniOrg\app\models\College;

/**
 * Class Dean
 *
 * @property int id
 * @property int member_id
 * @property int college_id
 *
 * @property Member member
 * @property College college
 *
 * @package Yeltrik\UniMbr\app\models
 */
class Dean extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'deans';

    public function college()
    {
        return $this->belongsTo(College::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public static function newFactory()
    {
        return new DeanFactory();
    }

}
