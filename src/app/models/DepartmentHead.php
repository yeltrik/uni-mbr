<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\UniMbr\database\factories\DepartmentHeadFactory;
use Yeltrik\UniOrg\app\models\College;
use Yeltrik\UniOrg\app\models\Department;

/**
 * Class DepartmentHead
 *
 * @property int id
 * @property int member_id
 * @property int department_id
 *
 * @property Member member
 * @property Department department
 *
 * @package Yeltrik\UniMbr\app\models
 */
class DepartmentHead extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'department_heads';

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public static function newFactory()
    {
        return new DepartmentHeadFactory();
    }

}
