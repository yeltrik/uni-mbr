<?php

namespace Yeltrik\UniMbr\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Ranks
 *
 * @property int id
 * @property string name
 *
 * @package App\Models
 */
class Rank extends Model
{
    use HasFactory;

    protected $connection = 'uni_mbr';
    public $table = 'ranks';

    /**
     * @return HasMany
     */
    public function faculty()
    {
        return $this->hasMany(Faculty::class);
    }

}
