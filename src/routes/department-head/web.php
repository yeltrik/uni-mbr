<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniMbr\app\http\controllers\DepartmentHeadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/department-head/',
    [DepartmentHeadController::class, 'index'])
    ->name('department-heads.index');

Route::get('/department-head/{departmentHead}',
    [DepartmentHeadController::class, 'show'])
    ->name('department-heads.show');
