<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniMbr\app\http\controllers\MemberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/member/',
    [MemberController::class, 'index'])
    ->name('members.index');

Route::get('/member/{member}',
    [MemberController::class, 'show'])
    ->name('members.show');
