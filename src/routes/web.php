<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require 'dean/web.php';
require 'department-head/web.php';
require 'faculty/web.php';
require 'member/web.php';
require 'staff/web.php';
require 'student/web.php';
