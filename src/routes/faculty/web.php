<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniMbr\app\http\controllers\FacultyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/faculty/',
    [FacultyController::class, 'index'])
    ->name('faculty.index');

Route::get('/faculty/{faculty}',
    [FacultyController::class, 'show'])
    ->name('faculty.show');
