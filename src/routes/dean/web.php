<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniMbr\app\http\controllers\DeanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/dean/',
    [DeanController::class, 'index'])
    ->name('deans.index');

Route::get('/dean/{dean}',
    [DeanController::class, 'show'])
    ->name('deans.show');
