<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\UniMbr\app\http\controllers\StaffController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/staff/',
    [StaffController::class, 'index'])
    ->name('staff.index');

Route::get('/staff/{staff}',
    [StaffController::class, 'show'])
    ->name('staff.show');
